"use strict";

class SchemaHandler {
  constructor(logger) {
    this.log = logger;
  }
}

export class ContentDirectory extends SchemaHandler {
  constructor(logger) {
    super(logger);
  }

  serviceEvent(service) {
    this.log.info(`service ${service.serviceType} found`);
		service.on("disappear", (service) => {
			this.log.info(`service ${service.serviceType} disappeared`);
		});
		// Bind to service to be able to call service actions
		service.bind( (service) => {
			// Call UPnP action SetTarget with parameter NewTargetValue
			this.log.info({service: service}, "bind callback");
			/*service.SetTarget({
				NewTargetValue: 1
			},function(res){
				console.log("Result",res);
			});*/
		}).on("event", (data) => {
			this.log.info({data: data});
		});
		// unsubscribe from the service after 10 seconds
		setTimeout(function(){
			service.removeAllListeners("event");
		},10000);
  }
}

export class ConnectionManager extends SchemaHandler {
  constructor(logger) {
    super(logger);
  }

  serviceEvent(service) {
    this.log.info("service "+service.serviceType+" found");
		service.on("disappear",(service) => {
			this.log.info(`service ${service.serviceType} disappeared`);
		});
		// Bind to service to be able to call service actions
		service.bind((service) => {
			// Call UPnP action SetTarget with parameter NewTargetValue
			/*service.SetTarget({
				NewTargetValue: 1
			},function(res){
				console.log("Result",res);
			});*/
		}).on("event",(data) => {
			this.log.info({data: data});
		});
		// unsubscribe from the service after 10 seconds
		setTimeout(() => {
			service.removeAllListeners("event");
		},10000);
  }
}
