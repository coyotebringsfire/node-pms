#!/usr/bin/env node
"use strict";

import { ContentDirectory, ConnectionManager } from "lib/Schema";

var upnp = require("peer-upnp"),
		http = require("http"),
		server = http.createServer(),
		bunyan = require('bunyan'),
		argv = require('yargs')
			.alias('v', 'verbose')
			.argv,
		log = bunyan.createLogger({
			name: "PMSJS",
			streams: [
		    {
		      level: 'info',
		      stream: process.stdout            // log INFO and above to stdout
		    },
		    {
		      level: 'error',
		      path: '/var/tmp/pms-error.log'  // log ERROR and above to a file
		    }
		  ],
			serializers: bunyan.stdSerializers,
			src: argv.verbose ? true : false
		});

const PORT = 8080;
const CD = new ContentDirectory(log);
const CM = new ConnectionManager(log);

server.listen(PORT);
// Peer is an event emitter
var peer = upnp.createPeer({
	prefix: "/upnp",
	server: server
}).on("ready",function(peer){
	log.info("ready");
	// listen to urn:schemas-upnp-org:service:ContentDirectory:1 services
	peer.on("urn:schemas-upnp-org:service:ContentDirectory:1", CD.serviceEvent)
			.on("urn:schemas-upnp-org:service:org:service:ConnectionManager:1", CM.serviceEvent)
			.on("upnp:rootdevice",function(device){ // listen to root devices
				log.info(`rootdevice ${device.deviceType} found`);
				device.on("disappear",function(device){
					log.info(`rootdevice ${device.UDN} disappeared`);
				});
			});
	// close peer after 30 seconds
	setTimeout(function(){
		peer.close();
	},30000);
}).on("close",function(){
	log.info("closed");
});
